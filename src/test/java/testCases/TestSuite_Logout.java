package testCases;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import testBase.TestBase;

public class TestSuite_Logout extends TestBase {
	final static Logger logger = Logger.getLogger(TestSuite_Logout.class);
	String baseUrl = "";
	String responseStatusSuccess ="";
	String responseStatusFailed = "";
	String endpointLogin = "/member/user/login";
	String endpointLogout = "/member/user/logout";
	
	@BeforeClass
	@Parameters({"baseUrl", "responseStatusSuccess", "responseStatusFailed"})
	void startTest(String definedBaseUrl, String definedResponseStatusSuccess, String definedResponseStatusFailed) {
		baseUrl = definedBaseUrl;
		responseStatusSuccess = definedResponseStatusSuccess;
		responseStatusFailed = definedResponseStatusFailed;
		logger.info("[ --- Started TestSuite_Logout --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void Logout_TSC_001() throws InterruptedException, ParseException{
//		- User already registered
//		- User logged in
		
		logger.info("[ --- Started Logout_TSC_001 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		logger.info(" === Login Process === ");
		RequestParams.put("phone", "087883445469");
		RequestParams.put("password", "Password@00");
		
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointLogin);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseHeader = response.getHeader("Set-Cookie");
		String responseBody = response.getBody().asString();
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		String cookieId = between(responseHeader, "=", "; Path=/; Secure; HttpOnly");
		logger.info("Response Header : " + responseHeader);
		logger.info("Response Body : " + responseBody);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		
		
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		
		Assert.assertEquals(responseDescription, "Success");
		Assert.assertEquals(responseCode, "200");
		Assert.assertEquals(tempData.get("name").toString(), "Michal Scott");
		Assert.assertEquals(tempData.get("phone").toString(), "087883445469");
		Assert.assertEquals(tempData.get("email").toString(), "michael@dundermiflin.com");
		Assert.assertEquals(Integer.parseInt(tempData.get("balance").toString()), 20000);

		if(responseCode.toString().equals("200")) {
			logger.info("Login Success using account : ");
			logger.info("Name : "+tempData.get("name".toString()));
			logger.info("Phone : "+tempData.get("phone".toString()));
			logger.info("email : "+tempData.get("email".toString()));
			logger.info("Balance : "+tempData.get("balance".toString()));
		}else {
			logger.info("Login failed");
		}	
		
		logger.info(" === Logout Process === ");
		RequestParams = new JSONObject();
		RequestParams.put("userId", 1);
		
		httpRequest.cookie("JSESSIONID", cookieId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointLogout);
		
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		System.out.println(response.getBody().asString());
		responseBody = response.getBody().asString();
		responseCode = json.get("status").toString();
		responseDescription = json.get("message").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "LOGGED_OUT_SUCCESSFUL");
		Assert.assertEquals(responseCode.toString(), "200");
		Thread.sleep(5);
		logger.info("[ --- Finished Logout_TSC_001 --- ]");
	}
	
	static String between(String value, String a, String b) {
        // Return a substring between the two strings.
        int posA = value.indexOf(a);
        if (posA == -1) {
            return "";
        }
        int posB = value.lastIndexOf(b);
        if (posB == -1) {
            return "";
        }
        int adjustedPosA = posA + a.length();
        if (adjustedPosA >= posB) {
            return "";
        }
        return value.substring(adjustedPosA, posB);
    }
	
	@AfterClass
	void tearDown() {
		logger.info("[ --- Finished TestSuite_Logout --- ]");
		System.out.println();
	}
}