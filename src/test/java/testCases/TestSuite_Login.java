package testCases;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import testBase.TestBase;

public class TestSuite_Login extends TestBase {
	final static Logger logger = Logger.getLogger(TestSuite_Login.class);
	String baseUrl = "";
	String responseStatusSuccess ="";
	String responseStatusFailed = "";
	String endpointLogin = "/member/user/login";
	
	@BeforeClass
	@Parameters({"baseUrl", "responseStatusSuccess", "responseStatusFailed"})
	void startTest(String definedBaseUrl, @Optional("Optional Parameter")  String definedResponseStatusSuccess, String definedResponseStatusFailed) {
		baseUrl = definedBaseUrl;
		responseStatusSuccess = definedResponseStatusSuccess;
		responseStatusFailed = definedResponseStatusFailed;
		logger.info("[ --- Started TestSuite_Login --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void Login_TSC_001() throws InterruptedException, ParseException{
//		Phone filled
//		Password Filled
//		Phone and Password match
		
		logger.info("[ --- Started Login_TSC_001 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("phone", "087883445469");
		RequestParams.put("password", "Password@00");
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointLogin);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseBody = response.getBody().asString();
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		
		Assert.assertEquals(responseDescription, "Success");
		Assert.assertEquals(responseCode, "200");
		Assert.assertEquals(tempData.get("name").toString(), "Michal Scott");
		Assert.assertEquals(tempData.get("phone").toString(), "087883445469");
		Assert.assertEquals(tempData.get("email").toString(), "michael@dundermiflin.com");
		Assert.assertEquals(Integer.parseInt(tempData.get("balance").toString()), 20000);

		if(responseCode.toString().equals("200")) {
			logger.info("Login Success");
			logger.info("Name : "+tempData.get("name".toString()));
			logger.info("Phone : "+tempData.get("phone".toString()));
			logger.info("email : "+tempData.get("email".toString()));
			logger.info("Balance : "+tempData.get("balance".toString()));
		}else {
			logger.info("Login failed");
		}	
		
		Thread.sleep(5);
		logger.info("[ --- Finished Login_TSC_001 --- ]");
	}
	
	@AfterClass
	void tearDown() {
		logger.info("[ --- Finished TestSuite_Login --- ]");
		System.out.println();
	}
}