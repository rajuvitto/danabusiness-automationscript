package testCases;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import testBase.TestBase;

public class TestSuite_Register extends TestBase {
	final static Logger logger = Logger.getLogger(TestSuite_Register.class);
	String baseUrl = "";
	String responseStatusSuccess ="";
	String responseStatusFailed = "";
	String endpointRegister = "/member/user/register";
	
	@BeforeClass
	@Parameters({"baseUrl", "responseStatusSuccess", "responseStatusFailed"})
	void startTest(String definedBaseUrl, String definedResponseStatusSuccess, String definedResponseStatusFailed) {
		baseUrl = definedBaseUrl;
		responseStatusSuccess = definedResponseStatusSuccess;
		responseStatusFailed = definedResponseStatusFailed;
		logger.info("[ --- Started TestSuite_Register --- ]");
	}
	
	
	@SuppressWarnings("unchecked")
	@Test
	void Register_TSC_001() throws InterruptedException, ParseException{
//		Valid name
//		Valid Password
//		Password and Confirm Password is match
//		Valid Phone
//		Valid Email
//		Request Id filled and unique
		
		logger.info("[ --- Started Register_TSC_001 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("name", "Raju Hutauruk");
		RequestParams.put("password", "Password@00");
		RequestParams.put("confirmPassword", "Password@00");
		RequestParams.put("phone", "082362505777");
		RequestParams.put("email", "Raju@gmail.com");
		RequestParams.put("requestId", generateRequestId());
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRegister);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		
		Assert.assertEquals(responseDescription, "Success");
		Assert.assertEquals(responseCode, "200");
		Assert.assertEquals(tempData.get("name").toString(), "Raju Hutauruk");
		Assert.assertEquals(tempData.get("phone").toString(), "082362505777");
		Assert.assertEquals(tempData.get("email").toString(), "Raju@gmail.com");
		Assert.assertEquals(Integer.parseInt(tempData.get("balance").toString()), 0);

		if(responseCode.toString().equals("200")) {
			logger.info("Register user success");
			logger.info("User ID : "+json.get("userId").toString());
			logger.info("Name : "+tempData.get("name".toString()));
			logger.info("Phone : "+tempData.get("phone".toString()));
			logger.info("email : "+tempData.get("email".toString()));
			logger.info("Balance : "+tempData.get("balance".toString()));
		}else {
			logger.info("Register user failed");
		}	
		
		Thread.sleep(5);
		logger.info("[ --- Finished Register_TSC_001 --- ]");
	}
	
	@AfterClass
	void tearDown() {
		logger.info("[ --- Finished TestSuite_Register --- ]");
		System.out.println();
	}
}