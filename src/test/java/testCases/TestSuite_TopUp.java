package testCases;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import testBase.TestBase;

public class TestSuite_TopUp extends TestBase {
	final static Logger logger = Logger.getLogger(TestSuite_TopUp.class);
	String baseUrl = "";
	String endpointTopUp = "/payment/topup";
	String responseStatusSuccess ="";
	String responseStatusFailed = "";
	
	@BeforeClass
	@Parameters({"baseUrl", "responseStatusSuccess", "responseStatusFailed"})
	void startTest(String definedBaseUrl, String definedResponseStatusSuccess, String definedResponseStatusFailed) {
		baseUrl = definedBaseUrl;
		responseStatusSuccess = definedResponseStatusSuccess;
		responseStatusFailed = definedResponseStatusFailed;
		logger.info("[ --- Started TestSuite_TopUp --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void TopUp_TSC_001() throws InterruptedException, ParseException{
//		- User already registered
//		- User logged in
//		- Current balance under Rp.2.000.000
//		- Correct amount to top up Rp.1000
//		- Balance after top up is or under Rp.2.000.000
//		- Idempotency key is filled and unique
		logger.info("[ --- Started TopUp_TSC_001 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("amount", "1000");
		RequestParams.put("requestId", generateRequestId());
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusSuccess);
		Assert.assertEquals(responseCode.toString(), "300");
		Thread.sleep(5);
		logger.info("[ --- Finished TopUp_TSC_001 --- ]");
	}
	
	
	@SuppressWarnings("unchecked")
	@Test
	void TopUp_TSC_055() throws InterruptedException, ParseException{
//		- User already registered
//		- User not logged in
//		- Current balance under Rp.2.000.000
//		- Correct amount to top up Rp.1000
//		- Balance after top up is or under Rp.2.000.000
//		- Idempotency key is filled and unique
		
		logger.info("[ --- Started TopUp_TSC_055 --- ]");	
		
		RestAssured.baseURI = baseUrl;		
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("amount", "1000");
		RequestParams.put("requestId", generateRequestId());
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.PATCH, endpointTopUp);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseCode, "301");
		Assert.assertEquals(responseDescription, "USER_NOT_LOGGED_IN");
		
		Thread.sleep(5);
		logger.info("[ --- Finished TopUp_TSC_055 --- ]");
	}
	
	@AfterClass
	void tearDown() {
		logger.info("[ --- Finished TestSuite_TopUp --- ]");
		System.out.println();
	}
}